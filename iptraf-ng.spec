Name:           iptraf-ng
Summary:        A console-based network monitoring utility
Version:        1.2.0
Release:        1
License:        GPLv2+
URL:            https://github.com/%{name}/%{name}/
Source0:        https://github.com/%{name}/%{name}/archive/v%{version}.tar.gz
Source1:        %{name}-logrotate.conf
Source2:        %{name}-tmpfiles.conf

BuildRequires:  gcc ncurses-devel
Obsoletes:      iptraf < 3.1
Provides:       iptraf = 3.1

%description
IPTraf is a console-based network monitoring program for Linux that
displays information about IP traffic.  It returns such information as:

 * Current TCP connections
 * UDP, ICMP, OSPF, and other types of IP packets
 * Packet and byte counts on TCP connections
 * IP, TCP, UDP, ICMP, non-IP, and other packet and byte counts
 * TCP/UDP counts by ports
 * Packet counts by packet sizes
 * Packet and byte counts by IP address
 * Interface activity
 * Flag statuses on TCP packets
 * LAN station statistics

If the iptraf command is issued without any command-line options,
the program comes up in interactive mode, with the various facilities
accessed through the main menu.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
CFLAGS="-g -O2 -Wall -W -std=gnu99 -Werror=format-security %{optflags}" \
  LDFLAGS="$RPM_LD_FLAGS"
%make_build V=1

%install
%make_install prefix=%{_prefix}

# Delete files that are not in HTML or PNG format in Documentation.
find Documentation -type f ! -name "*.html" \
    -type f ! -name "*.png" \
    -type f ! -name "." \
    | xargs rm -f

install -D -m 0644 -p %{SOURCE1} %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
install -D -m 0644 %{SOURCE2} %{buildroot}%{_prefix}/lib/tmpfiles.d/%{name}.conf
install -d -m 0755 %{buildroot}%{_localstatedir}/{log,lib}/%{name}
install -d -m 0755 %{buildroot}/run/%{name}/

%files
%defattr(-,root,root)
%license LICENSE
%{_prefix}/lib/tmpfiles.d/%{name}.conf
%config(noreplace) %{_sysconfdir}/logrotate.d/%{name}
%{_sbindir}/*ng
%{_localstatedir}/{log,lib}/%{name}
%dir /run/%{name}/

%files help
%defattr(-,root,root)
%doc Documentation FAQ CHANGES README*
%{_mandir}/man8/*.8.gz

%changelog
* Wed Jul 22 2020 lunankun <lunankun@huawei.com> - 1.2.0-1
- Type:update
- Id:NA
- SUG:NA
- DESC:update to 1.2.0

* Fri Aug 30 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.4-21
- Package init
